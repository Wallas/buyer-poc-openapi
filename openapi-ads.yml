openapi: 3.0.0

info:
  title: OpenAPI Specification for Ads
  description: API Specification document of the Ads system
  termsOfService: 'http://www.yapo.cl/terms'
  contact:
    name: Buyer Team
    url: http://yapo.confluence.cl
  license:
    name: MIT
  version: '1.0'

externalDocs:
  description: More information about Ads API
  url: http://yapo.confluence.cl

servers:
  - url: http://localhost:1234/api/v1
    description: Production server

  - url: https://{hostname}:{port}/stagingapi/v1
    description: Development server
    variables:
      hostname:
        default: localhost
      port:
        enum: 
          - '1111'
          - '9999'
        default: '9999'

tags:
  - name: ad
    description: Operations about Ads
    externalDocs:
      description: More information about Ads
      url: http://yapo.confluence.cl/ads

paths:
  /ads:
    summary: Operations about Ads
    description: Contains the list of operations applied on ads
    get:
      tags:
        - ad
      summary: Get all Ads
      description: Returns the list of all ads present in the system
      operationId: getAds
      responses:
        '200':
          description: Successfully returned all ads entries
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Ad'
        '4XX':
          $ref: '#/components/responses/4xxResponse'
        '5XX':
          $ref: '#/components/responses/5xxResponse'
    post:
      tags:
        - ad
      summary: Add a new ad
      description: Adds a new ad entry to the system and returns the added ad
      operationId: addAd
      requestBody:
        description: The new ad to add
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AdBody'
      responses:
        '201':
          description: Ad entry is added successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Ad'
        '4XX':
          $ref: '#/components/responses/4xxResponse'
        '5XX':
          $ref: '#/components/responses/5xxResponse'
  /ads/{adId}:
    summary: Operations about individual Ad
    description: Contains the list of operations applied on ads
    get:
      tags:
        - ad
      summary: Get a specific ad
      description: Returns the ad details
      operationId: getAd
      parameters:
        - name: adId
          in: path
          description: The unique ad id
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Successfully returned the ad details
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Ad'
        '4XX':
          $ref: '#/components/responses/4xxResponse'
        '5XX':
          $ref: '#/components/responses/5xxResponse'
    put:
      tags:
        - ad
      summary: Update an existing ad
      description: Updates an ad entry in the system and returns the updated ad
      operationId: updateAd
      parameters:
        - name: adId
          in: path
          description: The unique ad id
          required: true
          schema:
            type: string
      requestBody:
        description: The ad details to update
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AdBody'
      responses:
        '200':
          description: Ad is updated successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Ad'
        '4XX':
          $ref: '#/components/responses/4xxResponse'
        '5XX':
          $ref: '#/components/responses/5xxResponse'
        default:
          $ref: '#/components/responses/DefaultResponse'
    delete:
      summary: Delete an ad entry
      description: Removes an ad entry from the system
      operationId: deleteAd
      tags:
        - ad
      parameters:
        - name: adId
          in: path
          description: The unique ad id
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Successfully deleted
        '4XX':
          $ref: '#/components/responses/4xxResponse'
        '5XX':
          $ref: '#/components/responses/5xxResponse'
        default:
          description: Success
          content:
            application/json:
              examples:
                deleteResponse:
                  value: Successfully deleted

components:
  schemas:
    User:
      type: object
      description: Represents an ad creator
      properties:
        userId:
          type: number
          description: Unique ID of the user
        name:
          type: string
          description: Name of the user
        email:
          type: string
          description: Email of the user
    AdParam:
      type: object
      description: Id and Value pair
      properties:
        id:
          type: string
          description: Parameter id 
        value:
          type: string
          description: Parameter value 
    Ad:
      type: object
      description: Represents an ad entry
      properties:
        adId:
          type: number
          description: Unique ID of the ad
        title:
          type: string
          description: Title of the ad
        price:
          type: number
          description: Price of the ad
        category:
          type: string
          description: Category of the ad
        description:
          type: string
          description: Description text of the ad
        region: 
          type: number
          description: Region code of the ad location
        commune: 
          type: number
          description: Commune code of the ad location
        listTime:
          type: string
          description: Listing Date for the ad
        status:
          type: string
          description: Ad status
        user:
          $ref: '#/components/schemas/User'
        params:
          type: array
          items:
            $ref: '#/components/schemas/AdParam'
      example: 
        adId: 101
        title: 'Car ad title'
        price: 1200
        category: 'cars'
        description: 'Nice car'
        region: 15
        commune: 243
        status: 'active'
        listDate: '2021-08-18T20:51:00+00:00'
        user: {
          userId: 1,
          name: 'pepito',
          email: 'pepito@yapo.cl'
        }
        params: [
          { id: 'currency', value: 'UF' },
          { id: 'geoposition', value: '-33.413304, -70.602465' },
        ]
    AdBody:
      type: object
      description: Represents an ad entry
      properties:
        title:
          type: string
          description: Title of the ad
        price:
          type: number
          description: Price of the ad
        category:
          type: string
          description: Category of the ad
        description:
          type: string
          description: Description text of the ad
        region: 
          type: number
          description: Region code name of the ad location
        commune: 
          type: number
          description: Commune code name of the ad location
        status:
          type: string
          description: Ad status
          enum:
            - inactive
            - deleted
            - active
            - inactive
      required:
        - title
        - price
        - category
        - description
      example: 
        title: 'Car ad title'
        price: 1200
        category: 'cars'
        description: 'Nice car'
        region: 15
        commune: 243
        status: 'active'
  responses:
    4xxResponse:
      description: Bad Request
    5xxResponse:
      description: Internal server error
    DefaultResponse:
      description: Success
  securitySchemes:
    BasicAuthentication:
      type: http
      scheme: basic
    BearerAuthentication:
      type: http
      scheme: bearer
              
security: 
  - BasicAuthentication: []